

angular.module('movie', [])
    .factory('$exceptionHandler', function () {
        return function errorCatcherHandler(exception, cause) {
	    
	}
})
        .controller('searchCtrl', ['$scope', '$http', '$log', function($scope, $http, $log) {
	    
	               
	    //liste des films initiliase par initMoviesList
	    $scope.moviesList = [];
	    //liste des favoris
	    $scope.listFavoris = [];

	    /**
	     * initialise le tableau des films repertorier 
	     * et le tableau des realisateurs
	     * @author kturki  
	    **/
	    $scope.initMoviesList = function (){
		$http.get('./data.json').success(function(data){
		    $scope.listDirectors = data;
		    $scope.listDirectors.forEach(function(director){
			$scope.moviesList.push(director.moviesList);
		    });
		}).error(function(data, status){
		    $log.info(data, status);		 			
		});
	    };
	    
	    //appel init list reals
	    $scope.initMoviesList();
	    
	    /**
	     * Mock renvoie true si l'utilisateur est logger
	     * @todo: implementer l'appel ajax au serveur
	     * @author kturki 
	     **/
	    $scope.isLogin = function(){
		return true;
	    };


	    /**
	     * ajoute un film au favoris de l'utilisateur
	     * @author kturki
	     **/
	    $scope.addFilmToFavoris = function(film){
		$log.debug("ajout favoris");
		($scope.listFavoris.indexOf(film) == -1) ?
		    $scope.listFavoris.push(film) : null;
	    }

	    /**
	     * supprimer le film passer en parametres des
	     * favoris
	     **/
	    $scope.supprFavoris = function(film){
		index = $scope.listFavoris.indexOf(film);
		$scope.listFavoris.splice(index,1);
	    };
       }]);

